# vim-reval

Reval plugin for vim in native vimscript. See information on Reval [here.](https://github.com/qualialabs/reval)

Must have `curl` installed to work. May or may not work on Windows or older versions of Vim :/

## Installing

#### With vim-plug

```vim
Plug 'https://gitlab.com/aaron.chen/vim-reval'
```

## Usage

To disable default mappings, add this to your vimrc:
```vim
let g:vim_reval_no_mappings = 1
```

Current mappings:
```vim
  "reval only this buffer
  nmap <Leader>rr  <Plug>(reval-current-buffer)

  "reval all visible buffers on all tabs
  nmap <Leader>rR  <Plug>(reval-all-visible-buffers)

  "reval every buffer including hidden ones
  nmap <Leader>brr <Plug>(reval-all-buffers)

  "reval every window (every visible buffer on current tab)
  nmap <Leader>wrr <Plug>(reval-window-buffers)

  "reval buffers in arglist
  nmap <Leader>arr <Plug>(reval-all-args)

```
