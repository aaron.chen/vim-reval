if exists("g:loaded_vim_reval") || &cp
  finish
endif
let g:loaded_vim_reval = 1

function! s:findClosestFileToCurrentBuffer(fileName)
  let l:dirWildcard = '%:h'
  while expand(l:dirWildcard) != '/'
    let l:dirWildcard = l:dirWildcard . ':h'
    if filereadable(expand(l:dirWildcard) . '/' . a:fileName)
      return expand(l:dirWildcard) . '/' . a:fileName
    endif
  endwhile
  throw 'FileNotFound: Cannot find closest ' . a:fileName . ' file.'
endfunction

function! s:getRevalInfo()
  try
    let l:revalRcPath = s:findClosestFileToCurrentBuffer('.revalrc')
    let l:revalRcDir = fnamemodify(l:revalRcPath, ':h')
    let l:url = readfile(l:revalRcPath)[0]

    return { 'url': l:url, 'root': l:revalRcDir }
  catch /^FileNotFound/
    return { 'url': 'localhost:3000' , 'root': '/' }
  endtry
endfunction

function! s:hotReloadFile(errorOnInvalidFile)
  if filereadable(expand('%'))
    let l:revalInfo = s:getRevalInfo()
    let l:url = l:revalInfo['url'] 
    let l:root = l:revalInfo['root'] 

    let l:cwd = getcwd()
    execute 'cd ' . l:root
    let l:relativePath = expand('%:.')
    execute 'cd ' . l:cwd

    let l:fullUrlPath = l:url . '/reval/reload?filePath=' . l:relativePath
    let l:bufferContent = join(getline(1, '$'), "\n")
    let l:bufferContentArg = shellescape(escape(l:bufferContent, '\'))

    let l:shellcommand = 'curl -X POST ' . shellescape(l:fullUrlPath) . ' -d ' . l:bufferContentArg

    let l:output = system(l:shellcommand)
    if v:shell_error != 0
      throw 'RevalError: File ' . expand('%:t') . ' failed to reval'
    endif
  else
    if a:errorOnInvalidFile
      throw 'InvalidFile: File ' . expand('%:t') . " is not a file that can be reval'd"
    endif
  endif
endfunction

function! s:revalSingleBuffer()
  call s:hotReloadFile(1)
  echo "Reval'd " . expand('%:t')
endfunction

function! s:revalMultipleBuffer(revaledWhat)
  call s:hotReloadFile(0)
  echo "Reval'd " . a:revaledWhat
endfunction

nnoremap <silent> <Plug>(reval-current-buffer)      :            call <SID>revalSingleBuffer()<CR>
nnoremap <silent> <Plug>(reval-all-visible-buffers) :tabdo windo call <SID>revalMultipleBuffer('all visible buffers')<CR>
nnoremap <silent> <Plug>(reval-all-buffers)         :bufdo!      call <SID>revalMultipleBuffer('all buffers')<CR>
nnoremap <silent> <Plug>(reval-window-buffers)      :windo       call <SID>revalMultipleBuffer('window buffers')<CR>
nnoremap <silent> <Plug>(reval-all-args)            :argdo       call <SID>revalMultipleBuffer('all buffers in arglist')<CR>

if !exists("g:vim_reval_no_mappings") || ! g:vim_reval_no_mappings
  "reval only this buffer
  nmap <Leader>rr  <Plug>(reval-current-buffer)

  "reval all visible buffers on all tabs
  nmap <Leader>rR  <Plug>(reval-all-visible-buffers)

  "reval every buffer including hidden ones
  nmap <Leader>brr <Plug>(reval-all-buffers)

  "reval every window (every visible buffer on current tab)
  nmap <Leader>wrr <Plug>(reval-window-buffers)

  "reval buffers in arglist
  nmap <Leader>arr <Plug>(reval-all-args)
end
